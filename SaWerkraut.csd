;//////////////////////////////////////////////////
;// Herman Ferreira Militao de Asevedo
;// 
;// Trabalho de Graduação
;// UnB - Ciencia da Computacao
;//
;// SaWerkraut
;//////////////////////////////////////////////////

<Cabbage>

;//////////////////////////////
;// MACROS

;// GUI Colors

#define DARKCOLOR colour(32, 36, 32)
#define DARKERCOLOR colour(24, 28, 24)
#define DARKESTCOLOR colour(12, 16, 12)

#define SLIDERCOLOR_1 colour(64, 64, 64), trackercolour(32, 128, 32)
#define ONOFFBUTTON_1 colour:0(32,48,32), colour:1(32,128,32)
#define TRIGGERBUTTON_1 colour:0(0,0,0), colour:1(0,0,0)
#define SLIDERCOLOR_2 colour(64, 64, 64), trackercolour(192, 192, 192)

;// GUI elements
#define CONTEXTBUTTON fontcolour:0(192,192,192), fontcolour:1(255,255,255), colour:0(0,0,0), colour:1(48,48,48)

;// Sizes

;// Initialize the WaveTables. 
#define WAVETABLE_SIZE 8192
#define PWS_START_VALUE 2048

;//////////////////////////////////////////////////
;// GUI
;//////////////////////////////////////////////////

form caption("SaWerkraut"), size(675, 500), pluginID("sWkt"), identchannel("form"), guirefresh(15), $DARKESTCOLOR

;//////////////////////////////
;// Navigator

image bounds(15, 10, 645, 30),  plant("NavigationSelectorPlant"), $DARKERCOLOR {
	button bounds(0  , 0, 70, 30), text("MAIN"), channel("chnNavMain"), radiogroup(10), value(1), $CONTEXTBUTTON
	button bounds(70 , 0, 70, 30), text("OSC1"), channel("chnNavOsc1"), radiogroup(10), $CONTEXTBUTTON
	button bounds(140, 0, 70, 30), text("OSC2"), channel("chnNavOsc2"), radiogroup(10), $CONTEXTBUTTON
	button bounds(210, 0, 70, 30), text("OSC3"), channel("chnNavOsc3"), radiogroup(10), $CONTEXTBUTTON	
	button bounds(280, 0, 70, 30), text("FLT1"), channel("chnNavFlt1"), radiogroup(10), $CONTEXTBUTTON
	button bounds(350, 0, 65, 30), text("FX"),   channel("chnNavFX"),   radiogroup(10), $CONTEXTBUTTON
}

;//////////////////////////////
;// Main Left

image bounds(15, 45, 65, 355),  plant("MainLeftPlant"), $DARKERCOLOR {
	rslider bounds(5, 5, 50, 50), text("Volume"), channel("chnMainVolume"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
	rslider bounds(5, 55, 50, 50), text("Pan"), channel("chnMainPan"), range(-1,1,0,1,0.01), $SLIDERCOLOR_1
	rslider bounds(5, 105, 50, 50), text("Transp."), channel("chnMainCoarse"), range(-24,24,0,1,1), $SLIDERCOLOR_1
	rslider bounds(5, 155, 50, 50), text("Pitch"), channel("chnMainPitch"), range(-50,50,0,1,1), $SLIDERCOLOR_1	
	rslider bounds(5, 205, 50, 50), text("Bend"), channel("chnMainPtcBnd"), range(0,12,2,1,0.1), $SLIDERCOLOR_1
		label bounds(5, 255, 50, 14), text("Amount"), align("centre"), fontstyle("plain")
	
	label bounds(5, 290, 50, 12), text("ADSR"), align("centre"), fontstyle("plain")
	button bounds(5 , 302, 27, 15), text("cpy"), channel("chnADSRCopy"), latched(0), value(0), $ONOFFBUTTON_1
	button bounds(33 , 302, 27, 15), text("pst"), channel("chnADSRPaste"), latched(0), value(0), $ONOFFBUTTON_1

	label bounds(5, 320, 50, 12), text("LFO"), align("centre"), fontstyle("plain")
	button bounds(5 , 332, 27, 15), text("cpy"), channel("chnLFOCopy"), latched(0), value(0), $ONOFFBUTTON_1
	button bounds(33 , 332, 27, 15), text("pst"), channel("chnLFOPaste"), latched(0), value(0), $ONOFFBUTTON_1
}	

;//////////////////////////////
;// Main
	
groupbox bounds(85, 45, 340, 175), identchannel("idMainPlant"), plant("MainPlant"), text("Main"), visible(0), $DARKCOLOR {
	
	label bounds(0, 25, 340, 12), text("Active Modules")

	button bounds(10 , 45, 60, 25), text("Osc 1"), channel("chnEnableOsc1"), value(1), $ONOFFBUTTON_1
	button bounds(80 , 45, 60, 25), text("Osc 2"), channel("chnEnableOsc2"), value(0), $ONOFFBUTTON_1
	button bounds(150, 45, 60, 25), text("Osc 3"), channel("chnEnableOsc3"), value(0), $ONOFFBUTTON_1	
	button bounds(220, 45, 60, 25), text("Flt 1"), channel("chnEnableFlt1"), value(0), $ONOFFBUTTON_1

	button bounds(70, 75, 200, 25), text("Disable Performance Triggers"), channel("chnDisablePerfChn"), value(0), $ONOFFBUTTON_1

	label bounds(0, 110, 340, 12), text("Presets")

	combobox bounds(10, 130, 150, 25), channel("chnPresetList"), populate("*.snaps","SaWerkraut Presets")
	filebutton bounds(165, 130, 60, 25), channel("chnPresetSave"), text("Save"), populate("*.snaps","SaWerkraut Presets"), mode("directory"), value(0), $ONOFFBUTTON_1
}

;//////////////////////////////
;// Oscillator 1

groupbox bounds(85, 45, 340, 160), identchannel("idOsc1Plant1"), plant("Osc1Plant1"), text("Oscillator 1"), visible(0), $DARKCOLOR {
	combobox bounds(10, 30, 70, 30), channel("chnOsc1WaveType"), items("Sine","Triangle","Saw","Square","Pulse"), value(1)	
	gentable bounds(85, 30, 150, 60), tablenumber(10), identchannel("idOsc1WaveTable")
	rslider bounds(10, 95, 50, 50), text("Volume"), channel("chnOsc1Vol"), identchannel("idOsc1Vol"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
	
	rslider bounds(235, 40, 50, 50), text("PWS"), channel("chnOsc1PWS"), identchannel("idOsc1PWS"), range(0,$WAVETABLE_SIZE,$PWS_START_VALUE,1,1), visible(0), $SLIDERCOLOR_1

	image bounds(85, 95, 150, 50), $DARKERCOLOR

	rslider bounds(85, 95, 35, 35), text(""), channel("chnOsc1FreqRatioMain"), identchannel("idOsc1FreqRatioMain"), range(0,32,1,1,1), visible(1), $SLIDERCOLOR_1
	rslider bounds(135, 95, 35, 35), text(""), channel("chnOsc1FreqRatioFine"), identchannel("idOsc1FreqRatioFine"), range(0,0.999,0,1,0.001), visible(1), $SLIDERCOLOR_1

	label bounds(90, 130, 100, 14), text("Freq Multiplier"), align("left"), fontstyle("plain")

	rslider bounds(185, 95, 50, 50), text("Offset"), channel("chnOsc1FreqOffset"), identchannel("idOsc1FreqOffset"), range(0,10000,0,1,0.01), visible(1), $SLIDERCOLOR_1

	rslider bounds(235, 95, 50, 50), text("Phase"), channel("chnOsc1Phase"), identchannel("idOsc1Phase"), range(0,1,0,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(285, 95, 50, 50), text("FM Mod"), channel("chnOsc1FMmod"), identchannel("idOsc1FMmod"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
}

image bounds(85, 205, 340, 30), identchannel("idOsc1Plant2"), plant("Osc1Plant2"), visible(0), $DARKCOLOR {
		button bounds(0 , 0, 70, 30), text("Volume"), channel("chnOsc1NavVolume"), value(1), radiogroup(20), $CONTEXTBUTTON
		button bounds(70, 0, 70, 30), text("Pan"), channel("chnOsc1NavPan"), radiogroup(20), $CONTEXTBUTTON
		button bounds(140, 0, 70, 30), text("Pitch"), channel("chnOsc1NavPitch"), radiogroup(20), $CONTEXTBUTTON
		button bounds(210, 0, 70, 30), text("FM Mod"), channel("chnOsc1NavFMMod"), radiogroup(20), $CONTEXTBUTTON
}

;// Volume

image bounds(85, 235, 340, 165), identchannel("idOsc1Plant3_Vol"), plant("Osc1Plant3_Vol"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc1VolA"), range(0,5,0.05,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc1VolD"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc1VolS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc1VolR"), range(0,10,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc1VolAmount"), range(0,1,1,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc1VolLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc1VolLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc1VolLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc1VolLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc1VolLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1		
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// Pan

image bounds(85, 235, 340, 165), identchannel("idOsc1Plant3_Pan"), plant("Osc1Plant3_Pan"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc1PanA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc1PanD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc1PanS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc1PanR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc1PanAmount"), range(-1,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc1PanLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc1PanLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc1PanLfoI"), range(-1,1,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc1PanLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc1PanLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

}

;// Pitch

image bounds(85, 235, 340, 165), identchannel("idOsc1Plant3_Pitch"), plant("Osc1Plant3_Pitch"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc1PtcA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc1PtcD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc1PtcS"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc1PtcR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc1PtcAmount"), range(0,24,0,1,0.1), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc1PtcLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc1PtcLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc1PtcLfoI"), range(-12,12,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc1PtcLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc1PtcLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// FM Modulation

image bounds(85, 235, 340, 165), identchannel("idOsc1Plant3_FMMod"), plant("Osc1Plant3_FMMod"), visible(0), $DARKCOLOR {

	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc1FMModA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc1FMModD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc1FMModS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc1FMModR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc1FMModAmount"), range(0,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc1FMModLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc1FMModLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc1FMModLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc1FMModLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc1FMModLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

}

;//////////////////////////////
;// Oscillator 2

groupbox bounds(85, 45, 340, 160), identchannel("idOsc2Plant1"), plant("Osc2Plant1"), text("Oscillator 2"), visible(0), $DARKCOLOR {
	combobox bounds(10, 30, 70, 30), channel("chnOsc2WaveType"), items("Sine","Triangle","Saw","Square","Pulse"), value(1)	
	gentable bounds(85, 30, 150, 60), tablenumber(11), identchannel("idOsc2WaveTable")
	rslider bounds(10, 95, 50, 50), text("Volume"), channel("chnOsc2Vol"), identchannel("idOsc2Vol"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
	
	rslider bounds(235, 40, 50, 50), text("PWS"), channel("chnOsc2PWS"), identchannel("idOsc2PWS"), range(0,$WAVETABLE_SIZE,$PWS_START_VALUE,1,1), visible(0), $SLIDERCOLOR_1

	image bounds(85, 95, 150, 50), $DARKERCOLOR

	rslider bounds(85, 95, 35, 35), text(""), channel("chnOsc2FreqRatioMain"), identchannel("idOsc2FreqRatioMain"), range(0,32,1,1,1), visible(1), $SLIDERCOLOR_1
	rslider bounds(135, 95, 35, 35), text(""), channel("chnOsc2FreqRatioFine"), identchannel("idOsc2FreqRatioFine"), range(0,0.999,0,1,0.001), visible(1), $SLIDERCOLOR_1

	label bounds(90, 130, 100, 14), text("Freq Multiplier"), align("left"), fontstyle("plain")

	rslider bounds(185, 95, 50, 50), text("Offset"), channel("chnOsc2FreqOffset"), identchannel("idOsc2FreqOffset"), range(0,10000,0,1,0.01), visible(1), $SLIDERCOLOR_1

	rslider bounds(235, 95, 50, 50), text("Phase"), channel("chnOsc2Phase"), identchannel("idOsc2Phase"), range(0,1,0,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(285, 95, 50, 50), text("FM Mod"), channel("chnOsc2FMmod"), identchannel("idOsc2FMmod"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1

}

image bounds(85, 205, 340, 30), identchannel("idOsc2Plant2"), plant("Osc2Plant2"), visible(0), $DARKCOLOR {
		button bounds(0 , 0, 70, 30), text("Volume"), channel("chnOsc2NavVolume"), value(1), radiogroup(21), $CONTEXTBUTTON
		button bounds(70, 0, 70, 30), text("Pan"), channel("chnOsc2NavPan"), radiogroup(21), $CONTEXTBUTTON
		button bounds(140, 0, 70, 30), text("Pitch"), channel("chnOsc2NavPitch"), radiogroup(21), $CONTEXTBUTTON
		button bounds(210, 0, 70, 30), text("FM Mod"), channel("chnOsc2NavFMMod"), radiogroup(21), $CONTEXTBUTTON
}

;// Volume

image bounds(85, 235, 340, 165), identchannel("idOsc2Plant3_Vol"), plant("Osc2Plant3_Vol"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc2VolA"), range(0,5,0.05,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc2VolD"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc2VolS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc2VolR"), range(0,10,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc2VolAmount"), range(0,1,1,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc2VolLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc2VolLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc2VolLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc2VolLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc2VolLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
		
}

;// Pan

image bounds(85, 235, 340, 165), identchannel("idOsc2Plant3_Pan"), plant("Osc2Plant3_Pan"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc2PanA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc2PanD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc2PanS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc2PanR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc2PanAmount"), range(-1,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc2PanLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc2PanLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc2PanLfoI"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc2PanLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc2PanLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

}

;// Pitch

image bounds(85, 235, 340, 165), identchannel("idOsc2Plant3_Pitch"), plant("Osc2Plant3_Pitch"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc2PtcA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc2PtcD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc2PtcS"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc2PtcR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc2PtcAmount"), range(0,24,0,1,0.1), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc2PtcLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc2PtcLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc2PtcLfoI"), range(-12,12,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc2PtcLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc2PtcLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// FM Modulation

image bounds(85, 235, 340, 165), identchannel("idOsc2Plant3_FMMod"), plant("Osc2Plant3_FMMod"), visible(0), $DARKCOLOR {

	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc2FMModA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc2FMModD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc2FMModS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc2FMModR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc2FMModAmount"), range(0,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc2FMModLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc2FMModLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc2FMModLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc2FMModLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc2FMModLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

}

;//////////////////////////////
;// Oscillator 3

groupbox bounds(85, 45, 340, 160), identchannel("idOsc3Plant1"), plant("Osc3Plant1"), text("Oscillator 3"), visible(0), $DARKCOLOR {
	combobox bounds(10, 30, 70, 30), channel("chnOsc3WaveType"), items("Sine","Triangle","Saw","Square","Pulse"), value(1)	
	gentable bounds(85, 30, 150, 60), tablenumber(12), identchannel("idOsc3WaveTable")
	rslider bounds(10, 95, 50, 50), text("Volume"), channel("chnOsc3Vol"), identchannel("idOsc3Vol"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
	
	rslider bounds(235, 40, 50, 50), text("PWS"), channel("chnOsc3PWS"), identchannel("idOsc3PWS"), range(0,$WAVETABLE_SIZE,$PWS_START_VALUE,1,1), visible(0), $SLIDERCOLOR_1

	image bounds(85, 95, 150, 50), $DARKERCOLOR

	rslider bounds(85, 95, 35, 35), text(""), channel("chnOsc3FreqRatioMain"), identchannel("idOsc3FreqRatioMain"), range(0,32,1,1,1), visible(1), $SLIDERCOLOR_1
	rslider bounds(135, 95, 35, 35), text(""), channel("chnOsc3FreqRatioFine"), identchannel("idOsc3FreqRatioFine"), range(0,0.999,0,1,0.001), visible(1), $SLIDERCOLOR_1

	label bounds(90, 130, 100, 14), text("Freq Multiplier"), align("left"), fontstyle("plain")

	rslider bounds(185, 95, 50, 50), text("Offset"), channel("chnOsc3FreqOffset"), identchannel("idOsc3FreqOffset"), range(0,10000,0,1,0.01), visible(1), $SLIDERCOLOR_1

	rslider bounds(235, 95, 50, 50), text("Phase"), channel("chnOsc3Phase"), identchannel("idOsc3Phase"), range(0,1,0,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(285, 95, 50, 50), text("FM Mod"), channel("chnOsc3FMmod"), identchannel("idOsc3FMmod"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1

}

image bounds(85, 205, 340, 30), identchannel("idOsc3Plant2"), plant("Osc3Plant2"), visible(0), $DARKCOLOR {
		button bounds(0 , 0, 70, 30), text("Volume"), channel("chnOsc3NavVolume"), value(1), radiogroup(22), $CONTEXTBUTTON
		button bounds(70, 0, 70, 30), text("Pan"), channel("chnOsc3NavPan"), radiogroup(22), $CONTEXTBUTTON
		button bounds(140, 0, 70, 30), text("Pitch"), channel("chnOsc3NavPitch"), radiogroup(22), $CONTEXTBUTTON
		button bounds(210, 0, 70, 30), text("FM Mod"), channel("chnOsc3NavFMMod"), radiogroup(22), $CONTEXTBUTTON
}

;// Volume

image bounds(85, 235, 340, 165), identchannel("idOsc3Plant3_Vol"), plant("Osc3Plant3_Vol"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc3VolA"), range(0,5,0.05,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc3VolD"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc3VolS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc3VolR"), range(0,10,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc3VolAmount"), range(0,1,1,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc3VolLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc3VolLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc3VolLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc3VolLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc3VolLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// Pan

image bounds(85, 235, 340, 165), identchannel("idOsc3Plant3_Pan"), plant("Osc3Plant3_Pan"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc3PanA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc3PanD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc3PanS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc3PanR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc3PanAmount"), range(-1,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc3PanLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc3PanLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc3PanLfoI"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc3PanLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc3PanLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

}

;// Pitch

image bounds(85, 235, 340, 165), identchannel("idOsc3Plant3_Pitch"), plant("Osc3Plant3_Pitch"), visible(0), $DARKCOLOR {
	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc3PtcA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc3PtcD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc3PtcS"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc3PtcR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc3PtcAmount"), range(0,24,0,1,0.1), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc3PtcLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc3PtcLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc3PtcLfoI"), range(-12,12,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc3PtcLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc3PtcLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// FM Modulation

image bounds(85, 235, 340, 165), identchannel("idOsc3Plant3_FMMod"), plant("Osc3Plant3_FMMod"), visible(0), $DARKCOLOR {

	label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnOsc3FMModA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnOsc3FMModD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnOsc3FMModS"), range(0,1,1,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnOsc3FMModR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnOsc3FMModAmount"), range(0,1,0,1,0.01), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnOsc3FMModLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnOsc3FMModLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnOsc3FMModLfoI"), range(-1,1,0.5,1,0.001), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnOsc3FMModLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnOsc3FMModLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;// Filter 1

groupbox bounds(85, 45, 340, 160), identchannel("idFlt1Plant1"), plant("Flt1Plant1"), text("Filter 1"), visible(0), $DARKCOLOR {
	label bounds(10, 30, 35, 15), text("SVF"), align("left")
	;combobox bounds(10, 30, 70, 30), channel("chnFlt1FilterType"), items("SVF"), value(1)

	rslider bounds(85, 95, 50, 50), text("CutOff"), channel("chnFlt1Cutoff"), range(0,10000,1000,0.5,1), visible(1), $SLIDERCOLOR_1
	rslider bounds(135, 95, 50, 50), text("Reso"), channel("chnFlt1Res"), range(0,1,0.1,0.5,0.01), visible(1), $SLIDERCOLOR_1

	vslider bounds(190, 25, 20, 120), text("L"), channel("chnFlt1SVFLow"), range(0, 1, 1, 1, 0.01)
	vslider bounds(215, 25, 20, 120), text("B"), channel("chnFlt1SVFBand"), range(0, 1, 0, 1, 0.01)
	vslider bounds(240, 25, 20, 120), text("H"), channel("chnFlt1SVFHigh"), range(0, 1, 0, 1, 0.01)

}

image bounds(85, 205, 340, 30), identchannel("idFlt1Plant2"), plant("Flt1Plant2"), visible(0), $DARKCOLOR {
		button bounds(0 , 0, 70, 30), text("CutOff"), channel("chnFlt1NavCutOff"), value(1), radiogroup(30), $CONTEXTBUTTON
		button bounds(70, 0, 70, 30), text("Reso"), channel("chnFlt1NavRes"), radiogroup(30), $CONTEXTBUTTON
}

image bounds(85, 235, 340, 165), identchannel("idFlt1Plant3_CutOff"), plant("Flt1Plant3_CutOff"), visible(0), $DARKCOLOR {
		label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnFlt1CutOffA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnFlt1CutOffD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnFlt1CutOffS"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnFlt1CutOffR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnFlt1CutOffAmount"), range(0,10000,0,0.5,1), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnFlt1CutOffLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnFlt1CutOffLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnFlt1CutOffLfoI"), range(-5000,5000,500,1,1), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnFlt1CutOffLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnFlt1CutOffLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

image bounds(85, 235, 340, 165), identchannel("idFlt1Plant3_Res"), plant("Flt1Plant3_Res"), visible(0), $DARKCOLOR {
		label bounds(5, 0, 340, 15), text("ADSR"), align("left")
		rslider bounds(0, 15, 50, 50), text("Attack"), channel("chnFlt1ResA"), range(0,5,0.1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(50, 15, 50, 50), text("Decay"),channel("chnFlt1ResD"), range(0,5,1,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 15, 50, 50), text("Sustain"), channel("chnFlt1ResS"), range(0,1,0.5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(150, 15, 50, 50), text("Release"), channel("chnFlt1ResR"), range(0,10,0.5,0.5,0.01), $SLIDERCOLOR_1
		rslider bounds(290, 15, 50, 50), text("Amount"), channel("chnFlt1ResAmount"), range(0,1,0,1,0.05), $SLIDERCOLOR_1

	label bounds(5, 80, 340, 15), text("LFO"), align("left")
		rslider bounds(0, 95, 50, 50), text("Attack"), channel("chnFlt1ResLfoA"), range(0,10,0,1,0.1), $SLIDERCOLOR_1
		rslider bounds(50, 95, 50, 50), text("Speed"),channel("chnFlt1ResLfoS"), range(0,20,5,1,0.01), $SLIDERCOLOR_1
		rslider bounds(100, 95, 50, 50), text("Intensity"), channel("chnFlt1ResLfoI"), range(-1,1,0.5,1,0.01), $SLIDERCOLOR_1
		combobox bounds(150, 100, 60, 30), channel("chnFlt1ResLfoWaveType"), items("Sine","Triangle","Saw","Square"), value(1)
			label bounds(150, 128, 60, 14), text("Type"), fontstyle("plain"), align("centre")
		button bounds(295, 100, 40, 20), channel("chnFlt1ResLfoOnOff"), text("",""), value(0), $ONOFFBUTTON_1
			label bounds(295, 128, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;//////////////////////////////
;// FX

groupbox bounds(85, 45, 340, 175), identchannel("idFXPlant"), plant("FXPlant"), text("Effects"), visible(0), $DARKCOLOR {

	label bounds(10, 30, 65, 15), text("Delay"), align("left")

	rslider bounds(10, 50, 50, 50), text("Level"), channel("chnFXDelLvl"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(60, 50, 50, 50), text("Fdbk"), channel("chnFXDelFdbk"), range(0,0.99,0.5,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(110, 50, 50, 50), text("Time"), channel("chnFXDelTime"), range(0.01,1,0.2,1,0.01), visible(1), $SLIDERCOLOR_1	
	rslider bounds(160, 50, 50, 50), text("Stereo"), channel("chnFXDelStOff"), range(-0.5,0.5,0,1,0.01), visible(1), $SLIDERCOLOR_1
	button bounds(295, 55, 40, 20), channel("chnFXDelOnOff"), text("",""), value(0), $ONOFFBUTTON_1
		label bounds(295, 83, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")

	label bounds(10, 100, 65, 15), text("Reverb"), align("left")

	rslider bounds(10, 120, 50, 50), text("Level"), channel("chnFXRevLvl"), range(0,1,1,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(60, 120, 50, 50), text("Size"), channel("chnFXRevSize"), range(0,0.990,0.75,1,0.01), visible(1), $SLIDERCOLOR_1
	rslider bounds(110, 120, 50, 50), text("Damp"), channel("chnFXRevDamp"), range(0,1,0.5,1,0.01), visible(1), $SLIDERCOLOR_1
	button bounds(295, 125, 40, 20), channel("chnFXRevOnOff"), text("",""), value(0), $ONOFFBUTTON_1
		label bounds(295, 153, 40, 14), text("On/Off"), fontstyle("plain"), align("centre")
}

;//////////////////////////////
;// FM/Output Map

groupbox bounds(430, 45, 230, 355), identchannel("idFM_OutputMapPlant"), plant("FM_OutputMapPlant"), text("Output Map"), visible(1), $DARKCOLOR {

	;// Oscillators FM / OUT

	label bounds(0, 25, 230, 15), text("Oscillators FM / OUT"), align("centre")

	;// Column Labels
	label bounds(20, 50, 35, 15), text("1"), align("centre")
	label bounds(60, 50, 35, 15), text("2"), align("centre")
	label bounds(100, 50, 35, 15), text("3"), align("centre")
	label bounds(140, 50, 35, 15), text("Pan"), align("centre")
	label bounds(180, 50, 35, 15), text("Vol"), align("centre")
	;// Line Labels
	label bounds(5, 75, 15, 15), text("1"), align("centre")
	label bounds(5, 125, 15, 15), text("2"), align("centre")
	label bounds(5, 175, 15, 15), text("3"), align("centre")
	
	;// FM Dials
	rslider bounds(20, 65, 35, 35), channel("chnFML1C1"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(60, 65, 35, 35), channel("chnFML1C2"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(100, 65, 35, 35), channel("chnFML1C3"), range(0,1,0,1,0.01), $SLIDERCOLOR_2

	rslider bounds(140, 65, 35, 35), channel("chnFML1Pan"), range(-1,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(180, 65, 35, 35), channel("chnFML1Vol"), range(0,1,1,1,0.01), $SLIDERCOLOR_2
	
	rslider bounds(20, 115, 35, 35), channel("chnFML2C1"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(60, 115, 35, 35), channel("chnFML2C2"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(100, 115, 35, 35), channel("chnFML2C3"), range(0,1,0,1,0.01), $SLIDERCOLOR_2

	rslider bounds(140, 115, 35, 35), channel("chnFML2Pan"), range(-1,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(180, 115, 35, 35), channel("chnFML2Vol"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	
	rslider bounds(20, 165, 35, 35), channel("chnFML3C1"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(60, 165, 35, 35), channel("chnFML3C2"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(100, 165, 35, 35), channel("chnFML3C3"), range(0,1,0,1,0.01), $SLIDERCOLOR_2	

	rslider bounds(140, 165, 35, 35), channel("chnFML3Pan"), range(-1,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(180, 165, 35, 35), channel("chnFML3Vol"), range(0,1,0,1,0.01), $SLIDERCOLOR_2

	;// Filters MIX / OUT

	label bounds(0, 205, 230, 15), text("Filters FM IN / OUT"), align("centre")

	rslider bounds(20, 230, 35, 35), channel("chnFltL1C1"), identchannel("idFltL1C1"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	label bounds(20, 260, 35, 15), text("1"), align("centre")
	rslider bounds(60, 230, 35, 35), channel("chnFltL1C2"), identchannel("idFltL1C2"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	label bounds(60, 260, 35, 15), text("2"), align("centre")
	rslider bounds(100, 230, 35, 35), channel("chnFltL1C3"), identchannel("idFltL1C3"), range(0,1,0,1,0.01), $SLIDERCOLOR_2
	label bounds(100, 260, 35, 15), text("3"), align("centre")

	rslider bounds(140, 230, 35, 35), channel("chnFltL1Pan"), range(-1,1,0,1,0.01), $SLIDERCOLOR_2
	rslider bounds(180, 230, 35, 35), channel("chnFltL1Vol"), range(0,1,0,1,0.01), $SLIDERCOLOR_2

}

;//////////////////////////////
;// KeyBoard
;//////////////////////////////

image bounds(15, 405, 645, 80),  plant("Keyboard"), $DARKERCOLOR {
	keyboard bounds(20, 0, 605, 80)
}

</Cabbage>

<CsoundSynthesizer>

<CsOptions>

-n -d -+rtmidi=NULL -M0 -m0d --midi-key=4 --midi-velocity-amp=5

</CsOptions>

<CsInstruments>

;//////////////////////////////
;//Initialize the global variables. 
sr = 44100
ksmps = 60
nchnls = 2
0dbfs = 1

;//////////////////////////////
;// Initialized WaveTables.

giWAVETABLE_SIZE init 8192

;Sine wave
giSine		ftgen 1, 0, giWAVETABLE_SIZE, 10, 1
;Triangle wave
giTriangle	ftgen 2, 0, giWAVETABLE_SIZE, 7,  0, giWAVETABLE_SIZE/4, 1, giWAVETABLE_SIZE/2, -1, giWAVETABLE_SIZE/4, 0
;Saw wave
giSaw		ftgen 3, 0, giWAVETABLE_SIZE, 7, -1, giWAVETABLE_SIZE, 1
;Square wave
giSquare	ftgen 4, 0, giWAVETABLE_SIZE, 7, 1, giWAVETABLE_SIZE/2, 1,0,-1, giWAVETABLE_SIZE/2,-1
;Pulse wave
giPulse		ftgen 5, 0, giWAVETABLE_SIZE, 7, 1, giWAVETABLE_SIZE/4, 1,0,-1, (3*giWAVETABLE_SIZE)/4,-1

;// Assign MIDI events from all channels to the Oscillators.

massign 0, 0
massign 1, "Master"

;// Generic Strings Variables. 

gSOsc1Param = ""
gSOsc2Param = ""
gSOsc3Param = ""
gSFlt1Param = ""

gSvisible1 = "visible(1)"
gSvisible0 = "visible(0)"

;// Initialize the Global LR Signals.

gaMainL init 0
gaMainR init 0

gaOsc1L init 0
gaOsc1R init 0

gaOsc2L init 0
gaOsc2R init 0

gaOsc3L init 0
gaOsc3R init 0

gaFlt1L init 0
gaFlt1R init 0

;// Initialize the Oscillator's Wavetables

giOscsWaveTable[] init 3

giOscsWaveTable[0] ftgen 10, 0, giWAVETABLE_SIZE, 10, 1
giOscsWaveTable[1] ftgen 11, 0, giWAVETABLE_SIZE, 10, 1
giOscsWaveTable[2] ftgen 12, 0, giWAVETABLE_SIZE, 10, 1

;// Initializing global Oscillators Controllers Returns

gkOscsEnv[] init 3
gkOscsPan[] init 3
gkOscsFreq[] init 3
gkOscsFMMod[] init 3
giOscsPhase[] init 3

;// Initializing global Filters Controllers Returns

gkFltrsCtof[] init 1
gkFltrsRes[] init 1
gkFltrsLow[] init 1
gkFltrsBand[] init 1
gkFltrsHigh[] init 1

;//////////////////////////////
;// Initializing global Oscillators and Filters channel names

gSOscChannels[][] init 3, 52
gSFltChannels[][] init 1, 26

giStringsInit init 1

;//////////////////////////////
;// OTHER

giUIUpdateDisable init 0

;//////////////////////////////
;// ADSR/LFO Copy Paste

giADSRCpyA init 0.05
giADSRCpyD init 0.1
giADSRCpyS init 1
giADSRCpyR init 0.1
giADSRCpyAmount init 1

giLFOCpyA init 0
giLFOCpyS init 5
giLFOCpyI init 0.5
giLFOCpyT init 0

giCurrentModule init 0
giCurrentSubModule init 0

giADSRSubModuleIndex init 0
giLFOSubModuleIndex init 0

giOsc1ActiveSubModule init 0
giOsc2ActiveSubModule init 0
giOsc3ActiveSubModule init 0
giFlt1ActiveSubModule init 0

;//////////////////////////////////////////////////
;// DEBUG CLICK

opcode DGCK, 0,0
		aX = 0.1
		outs  aX*0,-aX	
endop

opcode DGCK2, 0,0
		aX = 0.1
		outs aX,aX*0
endop

opcode CopyADSR, 0,0

	if (giCurrentModule <= 3 && giCurrentModule > 0) then

		if(giCurrentSubModule == 0) then ;// Volume
			giADSRSubModuleIndex = 2
		elseif(giCurrentSubModule == 1) then ;// Pan
			giADSRSubModuleIndex = 43
		elseif(giCurrentSubModule == 2) then ;// Pitch
			giADSRSubModuleIndex = 28
		elseif(giCurrentSubModule == 3) then ;// FM Mod
			giADSRSubModuleIndex = 33
		endif

		giADSRCpyA chnget gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex]
		giADSRCpyD chnget gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+1]
		giADSRCpyS chnget gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+2]		
		giADSRCpyR chnget gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+3]
		giADSRCpyAmount chnget gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+4]

	elseif (giCurrentModule == 4) then
		if(giCurrentSubModule == 0) then ;// Cutoff
			giADSRSubModuleIndex = 6
		elseif(giCurrentSubModule == 1) then ;// Resonance
			giADSRSubModuleIndex = 16
		endif

		giADSRCpyA chnget gSFltChannels[0][giADSRSubModuleIndex]
		giADSRCpyD chnget gSFltChannels[0][giADSRSubModuleIndex+1]
		giADSRCpyS chnget gSFltChannels[0][giADSRSubModuleIndex+2]		
		giADSRCpyR chnget gSFltChannels[0][giADSRSubModuleIndex+3]
		giADSRCpyAmount chnget gSFltChannels[0][giADSRSubModuleIndex+4]
	endif
endop

opcode PasteADSR, 0,0

	if (giCurrentModule <= 3 && giCurrentModule > 0) then

		if(giCurrentSubModule == 0) then ;// Volume
			giADSRSubModuleIndex = 2
		elseif(giCurrentSubModule == 1) then ;// Pan
			giADSRSubModuleIndex = 43
		elseif(giCurrentSubModule == 2) then ;// Pitch
			giADSRSubModuleIndex = 28
		elseif(giCurrentSubModule == 3) then ;// FM Mod
			giADSRSubModuleIndex = 33
		endif

		chnset giADSRCpyA, gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex]
		chnset giADSRCpyD, gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+1]
		chnset giADSRCpyS, gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+2]
		chnset giADSRCpyR, gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+3]
		chnset giADSRCpyAmount, gSOscChannels[giCurrentModule-1][giADSRSubModuleIndex+4]

	elseif (giCurrentModule == 4) then

		if(giCurrentSubModule == 0) then ;// Cutoff
			giADSRSubModuleIndex = 6
		elseif(giCurrentSubModule == 1) then ;// Resonance
			giADSRSubModuleIndex = 16
		endif

		chnset giADSRCpyA, gSFltChannels[0][giADSRSubModuleIndex]
		chnset giADSRCpyD, gSFltChannels[0][giADSRSubModuleIndex+1]
		chnset giADSRCpyS, gSFltChannels[0][giADSRSubModuleIndex+2]
		chnset giADSRCpyR, gSFltChannels[0][giADSRSubModuleIndex+3]
		chnset giADSRCpyAmount, gSFltChannels[0][giADSRSubModuleIndex+4]
	endif
endop

opcode CopyLFO, 0,0

	if (giCurrentModule <= 3 && giCurrentModule > 0) then

		if(giCurrentSubModule == 0) then ;// Volume
			giLFOSubModuleIndex = 8
		elseif(giCurrentSubModule == 1) then ;// Pan
			giLFOSubModuleIndex = 15
		elseif(giCurrentSubModule == 2) then ;// Pitch
			giLFOSubModuleIndex = 20
		elseif(giCurrentSubModule == 3) then ;// FM Mod
			giLFOSubModuleIndex = 39
		endif

		giLFOCpyA chnget gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex]
		giLFOCpyS chnget gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+1]
		giLFOCpyI chnget gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+2]		
		giLFOCpyT chnget gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+3]

	elseif (giCurrentModule == 4) then
		if(giCurrentSubModule == 0) then ;// Cutoff
			giLFOSubModuleIndex = 12
		elseif(giCurrentSubModule == 1) then ;// Resonance
			giLFOSubModuleIndex = 22
		endif

		giLFOCpyA chnget gSFltChannels[0][giLFOSubModuleIndex]
		giLFOCpyS chnget gSFltChannels[0][giLFOSubModuleIndex+1]
		giLFOCpyI chnget gSFltChannels[0][giLFOSubModuleIndex+2]		
		giLFOCpyT chnget gSFltChannels[0][giLFOSubModuleIndex+3]
	endif
endop

opcode PasteLFO, 0,0

	if (giCurrentModule <= 3 && giCurrentModule > 0) then

		if(giCurrentSubModule == 0) then ;// Volume
			giLFOSubModuleIndex = 8
		elseif(giCurrentSubModule == 1) then ;// Pan
			giLFOSubModuleIndex = 15
		elseif(giCurrentSubModule == 2) then ;// Pitch
			giLFOSubModuleIndex = 20
		elseif(giCurrentSubModule == 3) then ;// FM Mod
			giLFOSubModuleIndex = 39
		endif

		chnset giLFOCpyA, gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex]
		chnset giLFOCpyS, gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+1]
		chnset giLFOCpyI, gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+2]
		chnset giLFOCpyT, gSOscChannels[giCurrentModule-1][giLFOSubModuleIndex+3]		

	elseif (giCurrentModule == 4) then

		if(giCurrentSubModule == 0) then ;// Cutoff
			giLFOSubModuleIndex = 12
		elseif(giCurrentSubModule == 1) then ;// Resonance
			giLFOSubModuleIndex = 22
		endif

		chnset giLFOCpyA, gSFltChannels[0][giLFOSubModuleIndex]
		chnset giLFOCpyS, gSFltChannels[0][giLFOSubModuleIndex+1]
		chnset giLFOCpyI, gSFltChannels[0][giLFOSubModuleIndex+2]
		chnset giLFOCpyT, gSFltChannels[0][giLFOSubModuleIndex+3]
	endif
endop

;//////////////////////////////////////////////////
;// Context Selector

opcode stringsInit, 0, 0

	;// Oscillators

	;// index at 47

	;// Osc 1

	;// Main OSC_CHANNELS_MAIN
	gSOscChannels[0][0] = "chnOsc1WaveType"
	gSOscChannels[0][1] = "chnOsc1PWS"
	gSOscChannels[0][24] = "chnOsc1Vol"

	gSOscChannels[0][25] = "chnOsc1FreqRatioMain"
	gSOscChannels[0][26] = "chnOsc1FreqRatioFine"
	gSOscChannels[0][27] = "chnOsc1FreqOffset"

	gSOscChannels[0][48] = "chnOsc1Phase"
	gSOscChannels[0][49] = "chnOsc1FMmod"

	;// Volume OSC_CHANNELS_VOL
	gSOscChannels[0][2] = "chnOsc1VolA"
	gSOscChannels[0][3] = "chnOsc1VolD"
	gSOscChannels[0][4] = "chnOsc1VolS"
	gSOscChannels[0][5] = "chnOsc1VolR"
	gSOscChannels[0][6] = "chnOsc1VolAmount"

	gSOscChannels[0][7] = "chnOsc1VolLfoOnOff"
	gSOscChannels[0][8] = "chnOsc1VolLfoA"
	gSOscChannels[0][9] = "chnOsc1VolLfoS"
	gSOscChannels[0][10] = "chnOsc1VolLfoI"
	gSOscChannels[0][11] = "chnOsc1VolLfoWaveType"

	gSOscChannels[0][12] = "chnOsc1VolModX"
	gSOscChannels[0][13] = "chnOsc1VolModY"

	;// Pan OSC_CHANNELS_PAN
	gSOscChannels[0][43] = "chnOsc1PanA"
	gSOscChannels[0][44] = "chnOsc1PanD"
	gSOscChannels[0][45] = "chnOsc1PanS"
	gSOscChannels[0][46] = "chnOsc1PanR"
	gSOscChannels[0][47] = "chnOsc1PanAmount"

	gSOscChannels[0][14] = "chnOsc1PanLfoOnOff"
	gSOscChannels[0][15] = "chnOsc1PanLfoA"
	gSOscChannels[0][16] = "chnOsc1PanLfoS"
	gSOscChannels[0][17] = "chnOsc1PanLfoI"
	gSOscChannels[0][18] = "chnOsc1PanLfoWaveType"

	;// Pitch OSC_CHANNELS_PITCH
	gSOscChannels[0][28] = "chnOsc1PtcA"
	gSOscChannels[0][29] = "chnOsc1PtcD"
	gSOscChannels[0][30] = "chnOsc1PtcS"
	gSOscChannels[0][31] = "chnOsc1PtcR"
	gSOscChannels[0][32] = "chnOsc1PtcAmount"

	gSOscChannels[0][19] = "chnOsc1PtcLfoOnOff"
	gSOscChannels[0][20] = "chnOsc1PtcLfoA"
	gSOscChannels[0][21] = "chnOsc1PtcLfoS"
	gSOscChannels[0][22] = "chnOsc1PtcLfoI"
	gSOscChannels[0][23] = "chnOsc1PtcLfoWaveType"

	;// FM Mod OSC_CHANNELS_FM_MOD
	gSOscChannels[0][33] = "chnOsc1FMModA"
	gSOscChannels[0][34] = "chnOsc1FMModD"
	gSOscChannels[0][35] = "chnOsc1FMModS"
	gSOscChannels[0][36] = "chnOsc1FMModR"
	gSOscChannels[0][37] = "chnOsc1FMModAmount"

	gSOscChannels[0][38] = "chnOsc1FMModLfoOnOff"
	gSOscChannels[0][39] = "chnOsc1FMModLfoA"
	gSOscChannels[0][40] = "chnOsc1FMModLfoS"
	gSOscChannels[0][41] = "chnOsc1FMModLfoI"
	gSOscChannels[0][42] = "chnOsc1FMModLfoWaveType"

	;// Auxiliary Addresses
	gSOscChannels[0][50] = "idOsc1WaveTable"
	gSOscChannels[0][51] = "tablenumber(10)"

	;// Osc 2

	;// Main OSC_CHANNELS_MAIN
	gSOscChannels[1][0] = "chnOsc2WaveType"
	gSOscChannels[1][1] = "chnOsc2PWS"
	gSOscChannels[1][24] = "chnOsc2Vol"

	gSOscChannels[1][25] = "chnOsc2FreqRatioMain"
	gSOscChannels[1][26] = "chnOsc2FreqRatioFine"
	gSOscChannels[1][27] = "chnOsc2FreqOffset"

	gSOscChannels[1][48] = "chnOsc2Phase"
	gSOscChannels[1][49] = "chnOsc2FMmod"

	;// Volume OSC_CHANNELS_VOL
	gSOscChannels[1][2] = "chnOsc2VolA"
	gSOscChannels[1][3] = "chnOsc2VolD"
	gSOscChannels[1][4] = "chnOsc2VolS"
	gSOscChannels[1][5] = "chnOsc2VolR"
	gSOscChannels[1][6] = "chnOsc2VolAmount"

	gSOscChannels[1][7] = "chnOsc2VolLfoOnOff"
	gSOscChannels[1][8] = "chnOsc2VolLfoA"
	gSOscChannels[1][9] = "chnOsc2VolLfoS"
	gSOscChannels[1][10] = "chnOsc2VolLfoI"
	gSOscChannels[1][11] = "chnOsc2VolLfoWaveType"

	gSOscChannels[1][12] = "chnOsc2VolModX"
	gSOscChannels[1][13] = "chnOsc2VolModY"

	;// Pan OSC_CHANNELS_PAN
	gSOscChannels[1][43] = "chnOsc2PanA"
	gSOscChannels[1][44] = "chnOsc2PanD"
	gSOscChannels[1][45] = "chnOsc2PanS"
	gSOscChannels[1][46] = "chnOsc2PanR"
	gSOscChannels[1][47] = "chnOsc2PanAmount"

	gSOscChannels[1][14] = "chnOsc2PanLfoOnOff"
	gSOscChannels[1][15] = "chnOsc2PanLfoA"
	gSOscChannels[1][16] = "chnOsc2PanLfoS"
	gSOscChannels[1][17] = "chnOsc2PanLfoI"
	gSOscChannels[1][18] = "chnOsc2PanLfoWaveType"

	;// Pitch OSC_CHANNELS_PITCH
	gSOscChannels[1][28] = "chnOsc2PtcA"
	gSOscChannels[1][29] = "chnOsc2PtcD"
	gSOscChannels[1][30] = "chnOsc2PtcS"
	gSOscChannels[1][31] = "chnOsc2PtcR"
	gSOscChannels[1][32] = "chnOsc2PtcAmount"

	gSOscChannels[1][19] = "chnOsc2PtcLfoOnOff"
	gSOscChannels[1][20] = "chnOsc2PtcLfoA"
	gSOscChannels[1][21] = "chnOsc2PtcLfoS"
	gSOscChannels[1][22] = "chnOsc2PtcLfoI"
	gSOscChannels[1][23] = "chnOsc2PtcLfoWaveType"

	;// FM Mod OSC_CHANNELS_FM_MOD
	gSOscChannels[1][33] = "chnOsc2FMModA"
	gSOscChannels[1][34] = "chnOsc2FMModD"
	gSOscChannels[1][35] = "chnOsc2FMModS"
	gSOscChannels[1][36] = "chnOsc2FMModR"
	gSOscChannels[1][37] = "chnOsc2FMModAmount"

	gSOscChannels[1][38] = "chnOsc2FMModLfoOnOff"
	gSOscChannels[1][39] = "chnOsc2FMModLfoA"
	gSOscChannels[1][40] = "chnOsc2FMModLfoS"
	gSOscChannels[1][41] = "chnOsc2FMModLfoI"
	gSOscChannels[1][42] = "chnOsc2FMModLfoWaveType"

	;// Auxiliary Addresses
	gSOscChannels[1][50] = "idOsc2WaveTable"
	gSOscChannels[1][51] = "tablenumber(11)"

	;// Osc 3

	;// Main OSC_CHANNELS_MAIN
	gSOscChannels[2][0] = "chnOsc3WaveType"
	gSOscChannels[2][1] = "chnOsc3PWS"
	gSOscChannels[2][24] = "chnOsc3Vol"

	gSOscChannels[2][25] = "chnOsc3FreqRatioMain"
	gSOscChannels[2][26] = "chnOsc3FreqRatioFine"
	gSOscChannels[2][27] = "chnOsc3FreqOffset"

	gSOscChannels[2][48] = "chnOsc3Phase"
	gSOscChannels[2][49] = "chnOsc3FMmod"

	;// Volume OSC_CHANNELS_VOL
	gSOscChannels[2][2] = "chnOsc3VolA"
	gSOscChannels[2][3] = "chnOsc3VolD"
	gSOscChannels[2][4] = "chnOsc3VolS"
	gSOscChannels[2][5] = "chnOsc3VolR"
	gSOscChannels[2][6] = "chnOsc3VolAmount"

	gSOscChannels[2][7] = "chnOsc3VolLfoOnOff"
	gSOscChannels[2][8] = "chnOsc3VolLfoA"
	gSOscChannels[2][9] = "chnOsc3VolLfoS"
	gSOscChannels[2][10] = "chnOsc3VolLfoI"
	gSOscChannels[2][11] = "chnOsc3VolLfoWaveType"

	gSOscChannels[2][12] = "chnOsc3VolModX"
	gSOscChannels[2][13] = "chnOsc3VolModY"

	;// Pan OSC_CHANNELS_PAN
	gSOscChannels[2][43] = "chnOsc3PanA"
	gSOscChannels[2][44] = "chnOsc3PanD"
	gSOscChannels[2][45] = "chnOsc3PanS"
	gSOscChannels[2][46] = "chnOsc3PanR"
	gSOscChannels[2][47] = "chnOsc3PanAmount"

	gSOscChannels[2][14] = "chnOsc3PanLfoOnOff"
	gSOscChannels[2][15] = "chnOsc3PanLfoA"
	gSOscChannels[2][16] = "chnOsc3PanLfoS"
	gSOscChannels[2][17] = "chnOsc3PanLfoI"
	gSOscChannels[2][18] = "chnOsc3PanLfoWaveType"

	;// Pitch OSC_CHANNELS_PITCH
	gSOscChannels[2][28] = "chnOsc3PtcA"
	gSOscChannels[2][29] = "chnOsc3PtcD"
	gSOscChannels[2][30] = "chnOsc3PtcS"
	gSOscChannels[2][31] = "chnOsc3PtcR"
	gSOscChannels[2][32] = "chnOsc3PtcAmount"

	gSOscChannels[2][19] = "chnOsc3PtcLfoOnOff"
	gSOscChannels[2][20] = "chnOsc3PtcLfoA"
	gSOscChannels[2][21] = "chnOsc3PtcLfoS"
	gSOscChannels[2][22] = "chnOsc3PtcLfoI"
	gSOscChannels[2][23] = "chnOsc3PtcLfoWaveType"

	;// FM Mod OSC_CHANNELS_FM_MOD
	gSOscChannels[2][33] = "chnOsc3FMModA"
	gSOscChannels[2][34] = "chnOsc3FMModD"
	gSOscChannels[2][35] = "chnOsc3FMModS"
	gSOscChannels[2][36] = "chnOsc3FMModR"
	gSOscChannels[2][37] = "chnOsc3FMModAmount"

	gSOscChannels[2][38] = "chnOsc3FMModLfoOnOff"
	gSOscChannels[2][39] = "chnOsc3FMModLfoA"
	gSOscChannels[2][40] = "chnOsc3FMModLfoS"
	gSOscChannels[2][41] = "chnOsc3FMModLfoI"
	gSOscChannels[2][42] = "chnOsc3FMModLfoWaveType"

	;// Auxiliary Addresses
	gSOscChannels[2][50] = "idOsc3WaveTable"
	gSOscChannels[2][51] = "tablenumber(12)"

	;// Filters

	;// index at 25

	;// Flt 1

	gSFltChannels[0][0] = "chnFlt1FilterType"

	gSFltChannels[0][1] = "chnFlt1Cutoff"
	gSFltChannels[0][2] = "chnFlt1Res"
	gSFltChannels[0][3] = "chnFlt1SVFLow"
	gSFltChannels[0][4] = "chnFlt1SVFBand"
	gSFltChannels[0][5] = "chnFlt1SVFHigh"

	gSFltChannels[0][6] = "chnFlt1CutOffA"
	gSFltChannels[0][7] = "chnFlt1CutOffD"
	gSFltChannels[0][8] = "chnFlt1CutOffS"
	gSFltChannels[0][9] = "chnFlt1CutOffR"
	gSFltChannels[0][10] = "chnFlt1CutOffAmount"

	gSFltChannels[0][11] = "chnFlt1CutOffLfoOnOff"
	gSFltChannels[0][12] = "chnFlt1CutOffLfoA"
	gSFltChannels[0][13] = "chnFlt1CutOffLfoS"
	gSFltChannels[0][14] = "chnFlt1CutOffLfoI"
	gSFltChannels[0][15] = "chnFlt1CutOffLfoWaveType"

	gSFltChannels[0][16] = "chnFlt1ResA"
	gSFltChannels[0][17] = "chnFlt1ResD"
	gSFltChannels[0][18] = "chnFlt1ResS"
	gSFltChannels[0][19] = "chnFlt1ResR"
	gSFltChannels[0][20] = "chnFlt1ResAmount"

	gSFltChannels[0][21] = "chnFlt1ResLfoOnOff"
	gSFltChannels[0][22] = "chnFlt1ResLfoA"
	gSFltChannels[0][23] = "chnFlt1ResLfoS"
	gSFltChannels[0][24] = "chnFlt1ResLfoI"
	gSFltChannels[0][25] = "chnFlt1ResLfoWaveType"
	
endop

opcode changeOscWaveType, 0, i

		iIndex xin
		
		iWaveType chnget gSOscChannels[iIndex][0]
		
		if (iWaveType != 5) then			
			tablecopy giOscsWaveTable[iIndex], iWaveType
		else
			iWavePWS chnget gSOscChannels[iIndex][1]
			giOscsWaveTable[iIndex] ftgen 10 + iIndex, 0, giWAVETABLE_SIZE, 7, 1, iWavePWS , 1,0,-1, giWAVETABLE_SIZE - iWavePWS, -1
		endif
		
		chnset	gSOscChannels[iIndex][51],gSOscChannels[iIndex][50]
		;chnset	"tablenumber(10)","idchnOsc1WaveTable"
endop

instr GUIControl
	
	stringsInit

	kUIDisableTrig changed chnget:k("chnDisablePerfChn")

	if (kUIDisableTrig == 1) then
		reinit CHANGE_UI_ABLENESS

		CHANGE_UI_ABLENESS:
			giUIUpdateDisable chnget "chnDisablePerfChn"
		rireturn
	endif

	kOsc1ParTrig 	changed chnget:k("chnOsc1NavVolume"), \
					chnget:k("chnOsc1NavPan"), \
					chnget:k("chnOsc1NavPitch"), \
					chnget:k("chnOsc1NavFMMod")
			
	if (kOsc1ParTrig == 1) then
	
		DISABLE_OSC1_PARAM_PLANTS:
			chnset gSvisible0, "idOsc1Plant3_Vol"
			chnset gSvisible0, "idOsc1Plant3_Pan"
			chnset gSvisible0, "idOsc1Plant3_Pitch"
			chnset gSvisible0, "idOsc1Plant3_FMMod"	

			iParVol chnget "chnOsc1NavVolume"
			iParPan chnget "chnOsc1NavPan"
			iParPtc chnget "chnOsc1NavPitch"		
			iParMod chnget "chnOsc1NavFMMod"		
			
			if iParVol==1 then
				chnset gSvisible1, "idOsc1Plant3_Vol"
				gSOsc1Param strcpyk "idOsc1Plant3_Vol"
				giOsc1ActiveSubModule = 0
				giCurrentSubModule = 0
			endif		
					
			if iParPan==1 then			
				chnset gSvisible1, "idOsc1Plant3_Pan"
				gSOsc1Param strcpyk "idOsc1Plant3_Pan"
				giOsc1ActiveSubModule = 1
				giCurrentSubModule = 1
			endif
			
			if iParPtc==1 then			
				chnset gSvisible1, "idOsc1Plant3_Pitch"
				gSOsc1Param strcpyk "idOsc1Plant3_Pitch"
				giOsc1ActiveSubModule = 2
				giCurrentSubModule = 2
			endif
			
			if iParMod==1 then			
				chnset gSvisible1, "idOsc1Plant3_FMMod"
				gSOsc1Param strcpyk "idOsc1Plant3_FMMod"
				giOsc1ActiveSubModule = 3			
			endif

			giCurrentSubModule = giOsc1ActiveSubModule

		rireturn
		
		;DEBUG_CLICK
	
		reinit DISABLE_OSC1_PARAM_PLANTS
	endif

;//////////////////////////////////////////////////
			
	kOsc2ParTrig 	changed chnget:k("chnOsc2NavVolume"), \
					chnget:k("chnOsc2NavPan"), \
					chnget:k("chnOsc2NavPitch"), \
					chnget:k("chnOsc2NavFMMod")
			
	if (kOsc2ParTrig == 1) then
	
		DISABLE_OSC2_PARAM_PLANTS:
			chnset gSvisible0, "idOsc2Plant3_Vol"
			chnset gSvisible0, "idOsc2Plant3_Pan"
			chnset gSvisible0, "idOsc2Plant3_Pitch"
			chnset gSvisible0, "idOsc2Plant3_FMMod"			

			iParVol chnget "chnOsc2NavVolume"
			iParPan chnget "chnOsc2NavPan"
			iParPtc chnget "chnOsc2NavPitch"		
			iParMod chnget "chnOsc2NavFMMod"		
			
			if iParVol==1 then
				chnset gSvisible1, "idOsc2Plant3_Vol"
				gSOsc2Param strcpyk "idOsc2Plant3_Vol"
				giOsc2ActiveSubModule = 0
			endif		
					
			if iParPan==1 then			
				chnset gSvisible1, "idOsc2Plant3_Pan"
				gSOsc2Param strcpyk "idOsc2Plant3_Pan"
				giOsc2ActiveSubModule = 1
			endif
			
			if iParPtc==1 then			
				chnset gSvisible1, "idOsc2Plant3_Pitch"
				gSOsc2Param strcpyk "idOsc2Plant3_Pitch"
				giOsc2ActiveSubModule = 2
			endif
			
			if iParMod==1 then			
				chnset gSvisible1, "idOsc2Plant3_FMMod"
				gSOsc2Param strcpyk "idOsc2Plant3_FMMod"
				giOsc2ActiveSubModule = 3
			endif

			giCurrentSubModule = giOsc2ActiveSubModule
		rireturn
	
		;DEBUG_CLICK
	
		reinit DISABLE_OSC2_PARAM_PLANTS
		
	endif

;//////////////////////////////////////////////////
			
	kOsc3ParTrig 	changed chnget:k("chnOsc3NavVolume"), \
					chnget:k("chnOsc3NavPan"), \
					chnget:k("chnOsc3NavPitch"), \
					chnget:k("chnOsc3NavFMMod")
			
	if (kOsc3ParTrig == 1) then
	
		DISABLE_OSC3_PARAM_PLANTS:
			chnset gSvisible0, "idOsc3Plant3_Vol"
			chnset gSvisible0, "idOsc3Plant3_Pan"
			chnset gSvisible0, "idOsc3Plant3_Pitch"
			chnset gSvisible0, "idOsc3Plant3_FMMod"

			iParVol chnget "chnOsc3NavVolume"
			iParPan chnget "chnOsc3NavPan"
			iParPtc chnget "chnOsc3NavPitch"		
			iParMod chnget "chnOsc3NavFMMod"		
			
			if iParVol==1 then
				chnset gSvisible1, "idOsc3Plant3_Vol"
				gSOsc3Param strcpyk "idOsc3Plant3_Vol"
				giOsc3ActiveSubModule = 0
			endif		
					
			if iParPan==1 then			
				chnset gSvisible1, "idOsc3Plant3_Pan"
				gSOsc3Param strcpyk "idOsc3Plant3_Pan"
				giOsc3ActiveSubModule = 1
			endif
			
			if iParPtc==1 then			
				chnset gSvisible1, "idOsc3Plant3_Pitch"
				gSOsc3Param strcpyk "idOsc3Plant3_Pitch"
				giOsc3ActiveSubModule = 2
			endif
			
			if iParMod==1 then			
				chnset gSvisible1, "idOsc3Plant3_FMMod"
				gSOsc3Param strcpyk "idOsc3Plant3_FMMod"
				giOsc3ActiveSubModule = 3
			endif

			giCurrentSubModule = giOsc3ActiveSubModule
		rireturn
	
		;DEBUG_CLICK
	
		reinit DISABLE_OSC3_PARAM_PLANTS
		

	endif

;//////////////////////////////////////////////////

	kFlt1ParTrig 	changed chnget:k("chnFlt1NavCutOff"), \
					chnget:k("chnFlt1NavRes")
			
	if (kFlt1ParTrig == 1) then
	
		DISABLE_FLT_1_PARAM_PLANTS:
			chnset gSvisible0, "idFlt1Plant3_CutOff"
			chnset gSvisible0, "idFlt1Plant3_Res"

			iParCutOff chnget "chnFlt1NavCutOff"
			iParRes chnget "chnFlt1NavRes"
			
			if iParCutOff==1 then
				chnset gSvisible1, "idFlt1Plant3_CutOff"
				gSFlt1Param strcpyk "idFlt1Plant3_CutOff"
				giFlt1ActiveSubModule = 0
			endif		
					
			if iParRes==1 then			
				chnset gSvisible1, "idFlt1Plant3_Res"
				gSFlt1Param strcpyk "idFlt1Plant3_Res"
				giFlt1ActiveSubModule = 1
			endif

			giCurrentSubModule = giFlt1ActiveSubModule

		rireturn
	
		;DEBUG_CLICK
	
		reinit DISABLE_FLT_1_PARAM_PLANTS
		

	endif

;//////////////////////////////////////////////////


	kNavTrig 	changed chnget:k("chnNavMain"), \
				chnget:k("chnNavOsc1"), \
				chnget:k("chnNavOsc2"), \
				chnget:k("chnNavOsc3"), \
				chnget:k("chnNavFlt1"), \
				chnget:k("chnNavFX")
				
	if kNavTrig==1 then
	
		DISABLE_VISIBLE_NAV_PLANTS:
			chnset gSvisible0, "idMainPlant"
		
			chnset gSvisible0, "idOsc1Plant1"
			chnset gSvisible0, "idOsc1Plant2"
			chnset gSvisible0, "idOsc1Plant3_Vol"
			chnset gSvisible0, "idOsc1Plant3_Pan"
			chnset gSvisible0, "idOsc1Plant3_Pitch"
			chnset gSvisible0, "idOsc1Plant3_FMMod"			
			
			chnset gSvisible0, "idOsc2Plant1"
			chnset gSvisible0, "idOsc2Plant2"
			chnset gSvisible0, "idOsc2Plant3_Vol"
			chnset gSvisible0, "idOsc2Plant3_Pan"
			chnset gSvisible0, "idOsc2Plant3_Pitch"
			chnset gSvisible0, "idOsc2Plant3_FMMod"

			chnset gSvisible0, "idOsc3Plant1"
			chnset gSvisible0, "idOsc3Plant2"
			chnset gSvisible0, "idOsc3Plant3_Vol"
			chnset gSvisible0, "idOsc3Plant3_Pan"
			chnset gSvisible0, "idOsc3Plant3_Pitch"
			chnset gSvisible0, "idOsc3Plant3_FMMod"			

			chnset gSvisible0, "idFlt1Plant1"
			chnset gSvisible0, "idFlt1Plant2"
			chnset gSvisible0, "idFlt1Plant3_CutOff"
			chnset gSvisible0, "idFlt1Plant3_Res"

			chnset gSvisible0, "idFXPlant"

			iNavMain chnget "chnNavMain"
			iNavOsc1 chnget "chnNavOsc1"
			iNavOsc2 chnget "chnNavOsc2"
			iNavOsc3 chnget "chnNavOsc3"
			iNavFlt1 chnget "chnNavFlt1"
			iNavFX chnget "chnNavFX"
			
			if iNavMain==1 then
				chnset gSvisible1, "idMainPlant"
				giCurrentModule = 0		
			endif

			if iNavOsc1==1 then
				chnset gSvisible1, "idOsc1Plant1"
				chnset gSvisible1, "idOsc1Plant2"
				chnset gSvisible1, gSOsc1Param
				giCurrentModule = 1
				giCurrentSubModule = giOsc1ActiveSubModule
			endif

			if iNavOsc2==1 then
				chnset gSvisible1, "idOsc2Plant1"
				chnset gSvisible1, "idOsc2Plant2"
				chnset gSvisible1, gSOsc2Param
				giCurrentModule = 2
				giCurrentSubModule = giOsc2ActiveSubModule
			endif

			if iNavOsc3==1 then
				chnset gSvisible1, "idOsc3Plant1"
				chnset gSvisible1, "idOsc3Plant2"
				chnset gSvisible1, gSOsc3Param
				giCurrentModule = 3
				giCurrentSubModule = giOsc3ActiveSubModule
			endif			

			if iNavFlt1==1 then
				chnset gSvisible1, "idFlt1Plant1"
				chnset gSvisible1, "idFlt1Plant2"
				chnset gSvisible1, gSFlt1Param
				giCurrentModule = 4
				giCurrentSubModule = giFlt1ActiveSubModule
			endif

			if iNavFX==1 then
				chnset gSvisible1, "idFXPlant"
				giCurrentModule = 5
			endif
		rireturn
	
		reinit DISABLE_VISIBLE_NAV_PLANTS
	endif
	
;//////////////////////////////////////////////////

	kOsc1WTTrig 	changed chnget:k("chnOsc1WaveType"), \
							chnget:k("chnOsc1PWS")
	
	if kOsc1WTTrig==1 then
	
		reinit CWT1
		
		CWT1:	
			changeOscWaveType 0

			iParWT chnget "chnOsc1WaveType"
			
			if (iParWT == 5) then						
				chnset gSvisible1, "idOsc1PWS"
			else		
				chnset gSvisible0, "idOsc1PWS"
			endif	
		rireturn
		
	endif
	
	kOsc2WTTrig 	changed chnget:k("chnOsc2WaveType"), \
							chnget:k("chnOsc2PWS")
	
	if kOsc2WTTrig==1 then
	
		reinit CWT2
		
		CWT2:	
			changeOscWaveType 1

			iParWT chnget "chnOsc2WaveType"
			
			if (iParWT == 5) then						
				chnset gSvisible1, "idOsc2PWS"
			else		
				chnset gSvisible0, "idOsc2PWS"
			endif
		rireturn			
	endif

	kOsc3WTTrig 	changed chnget:k("chnOsc3WaveType"), \
							chnget:k("chnOsc3PWS")
	
	if kOsc3WTTrig==1 then
	
		reinit CWT3
		
		CWT3:	
			changeOscWaveType 2

			iParWT chnget "chnOsc3WaveType"
			
			if (iParWT == 5) then						
				chnset gSvisible1, "idOsc3PWS"
			else		
				chnset gSvisible0, "idOsc3PWS"
			endif
		rireturn
			
	endif

;//////////////////////////////////////////////////

	kADSRCopyTrig changed chnget:k("chnADSRCopy")

	if (kADSRCopyTrig == 1) then
		ADSR_COPY:
			CopyADSR
		rireturn

		reinit ADSR_COPY
	endif

	kADSRPasteTrig changed chnget:k("chnADSRPaste")

	if (kADSRPasteTrig == 1) then
		ADSR_PASTE:
			PasteADSR
		rireturn

		reinit ADSR_PASTE
	endif

	kLFOCopyTrig changed chnget:k("chnLFOCopy")

	if (kLFOCopyTrig == 1) then
		LFO_COPY:
			CopyLFO
		rireturn

		reinit LFO_COPY
	endif

	kLFOPasteTrig changed chnget:k("chnLFOPaste")

	if (kLFOPasteTrig == 1) then
		LFO_PASTE:
			PasteLFO
		rireturn

		reinit LFO_PASTE
	endif
endin

;//////////////////////////////////////////////////
;// Aux Opcodes

opcode ADSRGenerator, k, iiii
	iAttack, iDecay, iSustain, iRelease xin
	
	if iAttack == 0 then
		iAttack = 0.0001 ;// madsr doesn't work with attack 0
	endif
	
	kADSR madsr iAttack, iDecay, iSustain, iRelease

	xout kADSR
endop

;//////////////////////////////
;// Oscillators Parameters

instr Oscillator
	
	;//////////////////////////////
	;// CHANNEL GETTERS

	iMidiKey = p4
	iVelocity = p5
	iOscIndex = p6 - 1

	CHANGE_MAIN_PARAMETERS:
		iCoarse chnget "chnMainCoarse"
		iPitch chnget "chnMainPitch"
		iBendAmount	chnget "chnMainPtcBnd"
		iVol chnget gSOscChannels[iOscIndex][24]
		iFMMod chnget gSOscChannels[iOscIndex][49]
	rireturn
	
	iPhase chnget gSOscChannels[iOscIndex][48]

	;// MAIN PITCH

	iFreqRatioMain chnget gSOscChannels[iOscIndex][25]
	iFreqRatioFine chnget gSOscChannels[iOscIndex][26]
	iFreqOffset chnget gSOscChannels[iOscIndex][27]
	
	;// VOL ADSR	
	
	iVolA chnget gSOscChannels[iOscIndex][2]
	iVolD chnget gSOscChannels[iOscIndex][3]
	iVolS chnget gSOscChannels[iOscIndex][4]
	iVolR chnget gSOscChannels[iOscIndex][5]
	
	iVolADSRAmount chnget gSOscChannels[iOscIndex][6]
	
	;// VOL LFO

	iLFOVolOn chnget gSOscChannels[iOscIndex][7]
	
	CHANGE_VOL_LFO:

		iVolLfoA chnget gSOscChannels[iOscIndex][8]
		iVolLfoS chnget gSOscChannels[iOscIndex][9]
		iVolLfoI chnget gSOscChannels[iOscIndex][10]
		iVolLfoWT chnget gSOscChannels[iOscIndex][11]
		
		if (iVolLfoWT == 1) then
			iVolLfoType = 0
		elseif (iVolLfoWT == 2) then
			iVolLfoType = 1
		elseif (iVolLfoWT == 3) then
			iVolLfoType = 4
		elseif (iVolLfoWT == 4) then
			iVolLfoType = 2
		endif
	rireturn

	;// Pan ADSR	
	
	iPanA chnget gSOscChannels[iOscIndex][43]
	iPanD chnget gSOscChannels[iOscIndex][44]
	iPanS chnget gSOscChannels[iOscIndex][45]
	iPanR chnget gSOscChannels[iOscIndex][46]
	
	iPanADSRAmount chnget gSOscChannels[iOscIndex][47]

	;// PAN LFO

	iLFOPanOn chnget gSOscChannels[iOscIndex][14]
	
	CHANGE_PAN_LFO:

		iPanLfoA chnget gSOscChannels[iOscIndex][15]
		iPanLfoS chnget gSOscChannels[iOscIndex][16]
		iPanLfoI chnget gSOscChannels[iOscIndex][17]
		iPanLfoWT chnget gSOscChannels[iOscIndex][18]
	
		if (iPanLfoWT == 1) then
			iPanLfoType = 0
		elseif (iPanLfoWT == 2) then
			iPanLfoType = 1
		elseif (iPanLfoWT == 3) then
			iPanLfoType = 4
		elseif (iPanLfoWT == 4) then
			iPanLfoType = 2
		endif
	rireturn	

	;// PITCH ADSR	
	
	iPtcA chnget gSOscChannels[iOscIndex][28]
	iPtcD chnget gSOscChannels[iOscIndex][29]
	iPtcS chnget gSOscChannels[iOscIndex][30]
	iPtcR chnget gSOscChannels[iOscIndex][31]
	
	iPtcADSRAmount chnget gSOscChannels[iOscIndex][32]

	;// PITCH LFO

	iLFOPtcOn chnget gSOscChannels[iOscIndex][19]	
	
	CHANGE_PITCH_LFO:

		iPtcLfoA chnget gSOscChannels[iOscIndex][20]
		iPtcLfoS chnget gSOscChannels[iOscIndex][21]
		iPtcLfoI chnget gSOscChannels[iOscIndex][22]
		iPtcLfoWT chnget gSOscChannels[iOscIndex][23]
	
		if (iPtcLfoWT == 1) then
			iPtcLfoType = 0
		elseif (iPtcLfoWT == 2) then
			iPtcLfoType = 1
		elseif (iPtcLfoWT == 3) then
			iPtcLfoType = 4
		elseif (iPtcLfoWT == 4) then
			iPtcLfoType = 2
		endif
	rireturn

	;// FM MOD ADSR	
	
	iFMModA chnget gSOscChannels[iOscIndex][33]
	iFMModD chnget gSOscChannels[iOscIndex][34]
	iFMModS chnget gSOscChannels[iOscIndex][35]
	iFMModR chnget gSOscChannels[iOscIndex][36]
	
	iFMModADSRAmount chnget gSOscChannels[iOscIndex][37]

	;// FM MOD LFO

	iLFOFMModOn chnget gSOscChannels[iOscIndex][38]	
	
	CHANGE_FM_MOD_LFO:

		iFMModLfoA chnget gSOscChannels[iOscIndex][39]
		iFMModLfoS chnget gSOscChannels[iOscIndex][40]
		iFMModLfoI chnget gSOscChannels[iOscIndex][41]
		iFMModLfoWT chnget gSOscChannels[iOscIndex][42]
	
		if (iFMModLfoWT == 1) then
			iFMModLfoType = 0
		elseif (iFMModLfoWT == 2) then
			iFMModLfoType = 1
		elseif (iFMModLfoWT == 3) then
			iFMModLfoType = 4
		elseif (iFMModLfoWT == 4) then
			iFMModLfoType = 2
		endif
	rireturn
	
	;//////////////////////////////	
	;// TRIGGERS

	kTrigWaveType changed chnget:k(gSOscChannels[iOscIndex][0]), \
		chnget:k(gSOscChannels[iOscIndex][1])

	if (kTrigWaveType==1) then
		;//reinit CHANGE_WAVE_TYPE
	endif		

	if (giUIUpdateDisable == 0) then

		kTrigMain changed chnget:k("chnMainCoarse"), \
			chnget:k("chnMainPitch"), \
			chnget:k("chnMainPtcBnd"), \
			chnget:k(gSOscChannels[iOscIndex][24]), \
			chnget:k(gSOscChannels[iOscIndex][49])
		
		if (kTrigMain == 1) then
			reinit CHANGE_MAIN_PARAMETERS
		endif

		kTrigVolLFO changed chnget:k(gSOscChannels[iOscIndex][8]), \
			chnget:k(gSOscChannels[iOscIndex][9]), \
			chnget:k(gSOscChannels[iOscIndex][10]), \
			chnget:k(gSOscChannels[iOscIndex][11])
		
		if (kTrigVolLFO == 1) then		
			reinit CHANGE_VOL_LFO
		endif		

		kTrigPanLFO changed chnget:k(gSOscChannels[iOscIndex][15]), \
			chnget:k(gSOscChannels[iOscIndex][16]), \
			chnget:k(gSOscChannels[iOscIndex][17]), \
			chnget:k(gSOscChannels[iOscIndex][18])
		
		if (kTrigPanLFO == 1) then
			reinit CHANGE_PAN_LFO
		endif
		
		kTrigPtcLFO changed chnget:k(gSOscChannels[iOscIndex][20]), \
			chnget:k(gSOscChannels[iOscIndex][21]), \
			chnget:k(gSOscChannels[iOscIndex][22]), \
			chnget:k(gSOscChannels[iOscIndex][23])
		
		if (kTrigPtcLFO == 1) then
			reinit CHANGE_PITCH_LFO
		endif

		kTrigFMModLFO changed chnget:k(gSOscChannels[iOscIndex][39]), \
			chnget:k(gSOscChannels[iOscIndex][40]), \
			chnget:k(gSOscChannels[iOscIndex][41]), \
			chnget:k(gSOscChannels[iOscIndex][42])
		
		if (kTrigFMModLFO == 1) then
			reinit CHANGE_FM_MOD_LFO
		endif

	endif

	;//////////////////////////////	
	;// PARAMETERS GENERATION
	
	kMidiKey = iMidiKey + iCoarse
	kPtcBend pchbend
	kPtcBend = (kPtcBend + 1) * 0.5
	iFreqRatio = (iFreqRatioMain + iFreqRatioFine)
	
	if (iVolADSRAmount != 0) then
		kVolADSR ADSRGenerator iVolA, iVolD, iVolS, iVolR
		kVolADSR = (iVolADSRAmount) * kVolADSR + (1 - iVolADSRAmount)
	else
		kVolADSR = 1
	endif

	if (iPanADSRAmount != 0) then
		kPanADSR ADSRGenerator iPanA, iPanD, iPanS, iPanR
		kPanADSR = kPanADSR * iPanADSRAmount
	else
		kPanADSR = 0
	endif

	if (iPtcADSRAmount != 0) then
		kPtcADSR ADSRGenerator iPtcA, iPtcD, iPtcS, iPtcR
		kPtcADSR = (iPtcADSRAmount) * kPtcADSR - iPtcADSRAmount * 0.5
	else
		kPtcADSR = 0
	endif

	if (iFMModADSRAmount != 0) then
		kFMModADSR ADSRGenerator iFMModA, iFMModD, iFMModS, iFMModR
		kFMModADSR = kFMModADSR * iFMModADSRAmount + (1 - iFMModADSRAmount)
	else
		kFMModADSR = 1
	endif

	if (iLFOVolOn == 1) then
		kAttack linenr 1, iVolLfoA, 0, 0
		kVolLFO lfo 1, iVolLfoS, iVolLfoType
		
		if (iVolLfoType == 4) then
			kVolLFO = 2 * kVolLFO - 1; Saw WaveType is a half Saw
		endif
		
		kVolLFO = (iVolLfoI >= 0) ? 1 - ((kVolLFO + 1) * 0.5) * iVolLfoI * (kAttack) : (1-abs(iVolLfoI)) - ((kVolLFO + 1) * 0.5) * (abs(iVolLfoI)) * (kAttack)
		
	else
		kVolLFO = 1
	endif

	if (iLFOPanOn == 1) then

		kAttack linenr 1, iPanLfoA, 0, 0
		kPanLFO lfo 1, iPanLfoS, iPanLfoType
		
		if (iPanLfoType == 4) then
			kPanLFO = 2 * kPanLFO - 1; Saw WaveType is a half Saw
		endif
		
		kPanLFO = kPanLFO * iPanLfoI * kAttack
	else
		kPanLFO = 0
	endif

	if (iLFOPtcOn == 1) then

		kAttack linenr 1, iPtcLfoA, 0, 0
		kPtcLFO lfo 1, iPtcLfoS, iPtcLfoType
		
		if (iPtcLfoType == 4) then
			kPtcLFO = 2 * kPtcLFO - 1; Saw WaveType is a half Saw
		endif
		
		kPtcLFO = kPtcLFO * (iPtcLfoI) * kAttack
	else
		kPtcLFO = 0
	endif

	if (iLFOFMModOn == 1) then

		kAttack linenr 1, iFMModLfoA, 0, 0
		kFMModLFO lfo 1, iFMModLfoS, iFMModLfoType
		
		if (iFMModLfoType == 4) then
			kFMModLFO = 2 * kFMModLFO - 1; Saw WaveType is a half Saw
		endif
		
		kFMModLFO = kFMModLFO * (iFMModLfoI) * kAttack
	else
		kFMModLFO = 0
	endif

	kPan limit (kPanLFO + kPanADSR), -1, 1

	gkOscsEnv[iOscIndex] = kVolADSR * kVolLFO * iVol
	gkOscsPan[iOscIndex] = (kPan + 1) * 0.5
	gkOscsFreq[iOscIndex] = cpsmidinn((kMidiKey + kPtcLFO + (iPitch * 0.01) + kPtcBend * iBendAmount) + kPtcADSR) * iFreqRatio + iFreqOffset
	gkOscsFMMod[iOscIndex] = (kFMModADSR + kFMModLFO) * iFMMod 
	giOscsPhase[iOscIndex] init iPhase
	
	aNull init 0
	
	out aNull
endin

;//////////////////////////////
;// Filter Parameters

instr Filter

	;//////////////////////////////
	;// CHANNEL GETTERS

	iFltIndex = p6 - 1

	;// MAIN FILTER
	
	CHANGE_MAIN_FILTER:
		iCtof chnget gSFltChannels[iFltIndex][1]
		iRes chnget gSFltChannels[iFltIndex][2]
	
		iLow chnget gSFltChannels[iFltIndex][3]
		iBand chnget gSFltChannels[iFltIndex][4]
		iHigh chnget gSFltChannels[iFltIndex][5]
	rireturn
	
	;// CUTOFF ADSR	
	
	iCtofA chnget gSFltChannels[iFltIndex][6]
	iCtofD chnget gSFltChannels[iFltIndex][7]
	iCtofS chnget gSFltChannels[iFltIndex][8]
	iCtofR chnget gSFltChannels[iFltIndex][9]
	
	iCtofADSRAmount chnget gSFltChannels[iFltIndex][10]
	
	;// CUTOFF LFO

	iLFOCtofOn chnget gSFltChannels[iFltIndex][11]
	
	CHANGE_CUTOFF_LFO:

		iCtofLfoA chnget gSFltChannels[iFltIndex][12]
		iCtofLfoS chnget gSFltChannels[iFltIndex][13]
		iCtofLfoI chnget gSFltChannels[iFltIndex][14]
		iCtofLfoWT chnget gSFltChannels[iFltIndex][15]
		
		if (iCtofLfoWT == 1) then
			iCtofLfoType = 0
		elseif (iCtofLfoWT == 2) then
			iCtofLfoType = 1
		elseif (iCtofLfoWT == 3) then
			iCtofLfoType = 4
		elseif (iCtofLfoWT == 4) then
			iCtofLfoType = 2
		endif
	rireturn

	;// RESONANCE ADSR	
	
	iResA chnget gSFltChannels[iFltIndex][16]
	iResD chnget gSFltChannels[iFltIndex][17]
	iResS chnget gSFltChannels[iFltIndex][18]
	iResR chnget gSFltChannels[iFltIndex][19]
	
	iResADSRAmount chnget gSFltChannels[iFltIndex][20]

	;// RESONANCE LFO

	iLFOResOn chnget gSFltChannels[iFltIndex][21]
	
	CHANGE_RESONANCE_LFO:

		iResLfoA chnget gSFltChannels[iFltIndex][22]
		iResLfoS chnget gSFltChannels[iFltIndex][23]
		iResLfoI chnget gSFltChannels[iFltIndex][24]
		iResLfoWT chnget gSFltChannels[iFltIndex][25]
	
		if (iResLfoWT == 1) then
			iResLfoType = 0
		elseif (iResLfoWT == 2) then
			iResLfoType = 1
		elseif (iResLfoWT == 3) then
			iResLfoType = 4
		elseif (iResLfoWT == 4) then
			iResLfoType = 2
		endif
	rireturn	

	;//////////////////////////////	
	;// TRIGGERS

	if (giUIUpdateDisable == 0) then
	
		kTrigMainFlt changed chnget:k(gSFltChannels[iFltIndex][1]), \
			chnget:k(gSFltChannels[iFltIndex][2]), \
			chnget:k(gSFltChannels[iFltIndex][3]), \
			chnget:k(gSFltChannels[iFltIndex][4]), \
			chnget:k(gSFltChannels[iFltIndex][5])
		
		if (kTrigMainFlt == 1) then
			reinit CHANGE_MAIN_FILTER
		endif	
		
		kTrigCtofLFO changed chnget:k(gSFltChannels[iFltIndex][12]), \
			chnget:k(gSFltChannels[iFltIndex][13]), \
			chnget:k(gSFltChannels[iFltIndex][14]), \
			chnget:k(gSFltChannels[iFltIndex][15])
		
		if (kTrigCtofLFO == 1) then		
			reinit CHANGE_CUTOFF_LFO
		endif		

		kTrigResLFO changed chnget:k(gSFltChannels[iFltIndex][22]), \
			chnget:k(gSFltChannels[iFltIndex][23]), \
			chnget:k(gSFltChannels[iFltIndex][24]), \
			chnget:k(gSFltChannels[iFltIndex][25])
		
		if (kTrigResLFO == 1) then
			reinit CHANGE_RESONANCE_LFO
		endif	
	
	endif

	;//////////////////////////////	
	;// PARAMETERS GENERATION
	
	if (iCtofADSRAmount != 0) then
		kCtofADSR ADSRGenerator iCtofA, iCtofD, iCtofS, iCtofR
		kCtofADSR = kCtofADSR * (iCtofADSRAmount)
	else
		kCtofADSR = 0
	endif

	if (iResADSRAmount != 0) then
		kResADSR ADSRGenerator iResA, iResD, iResS, iResR
		kResADSR = kResADSR * iResADSRAmount
	else
		kResADSR = 0
	endif

	if (iLFOCtofOn == 1) then
		kAttack linenr 1, iCtofLfoA, 0, 0
		kCtofLFO lfo kAttack , iCtofLfoS, iCtofLfoType
		
		if (iCtofLfoType == 4) then
			kCtofLFO = 2 * kCtofLFO - 1; Saw WaveType is a half Saw
		endif		
		
		kCtofLFO = kCtofLFO * iCtofLfoI * kAttack

		if (iCtofLfoI < 0) then
			kCtofLFO = 1 - abs(iCtofLfoI) * kCtofLFO; Inverted WaveType
		endif
	else
		kCtofLFO = 0
	endif

	if (iLFOResOn == 1) then

		kAttack linenr 1, iResLfoA, 0, 0
		kResLFO lfo 1, iResLfoS, iResLfoType
		
		if (iResLfoType == 4) then
			kResLFO = 2 * kResLFO - 1; Saw WaveType is a half Saw
		endif
		
		kResLFO = kResLFO * iResLfoI * kAttack
	else
		kResLFO = 0
	endif

	kCtof limit (kCtofADSR + kCtofLFO) + iCtof, 0, 10000
	kRes limit (kResADSR + kResLFO) + iRes, 0, 1

	gkFltrsCtof[iFltIndex] = kCtof
	gkFltrsRes[iFltIndex] = kRes
	
	gkFltrsLow[iFltIndex] = iLow
	gkFltrsBand[iFltIndex] = iBand
	gkFltrsHigh[iFltIndex] = iHigh

	aNull init 0
	
	out aNull
endin

;//////////////////////////////////////////////////
;// Master

instr Master

	iCps = p4
	iAmp = p5

	iActiveOsc1 chnget "chnEnableOsc1"
	iActiveOsc2 chnget "chnEnableOsc2"
	iActiveOsc3 chnget "chnEnableOsc3"
	iActiveFlt1 chnget "chnEnableFlt1"
	
	;// Output Map
	
	CHANGE_FM_OSC1:
		iFML1C1Value chnget "chnFML1C1"
		iFML1C2Value chnget "chnFML1C2"
		iFML1C3Value chnget "chnFML1C3"

		iFML1C1Value = iFML1C1Value * 6000
		iFML1C2Value = iFML1C2Value * 6000
		iFML1C3Value = iFML1C3Value * 6000
	rireturn
	CHANGE_FM_OSC2:
		iFML2C1Value chnget "chnFML2C1"
		iFML2C2Value chnget "chnFML2C2"
		iFML2C3Value chnget "chnFML2C3"

		iFML2C1Value = iFML2C1Value * 6000
		iFML2C2Value = iFML2C2Value * 6000
		iFML2C3Value = iFML2C3Value * 6000
	rireturn
	CHANGE_FM_OSC3:
		iFML3C1Value chnget "chnFML3C1"
		iFML3C2Value chnget "chnFML3C2"
		iFML3C3Value chnget "chnFML3C3"

		iFML3C1Value = iFML3C1Value * 6000
		iFML3C2Value = iFML3C2Value * 6000
		iFML3C3Value = iFML3C3Value * 6000	
	rireturn
	CHANGE_FLT:
		iFltL1C1Value chnget "chnFltL1C1"
		iFltL1C2Value chnget "chnFltL1C2"
		iFltL1C3Value chnget "chnFltL1C3"
	rireturn

	if (giUIUpdateDisable == 0) then
		
		kTrigFM1 changed chnget:k("chnFML1C1"), \
			chnget:k("chnFML1C2"), \
			chnget:k("chnFML1C3")

		kTrigFM2 changed chnget:k("chnFML2C1"), \
			chnget:k("chnFML2C2"), \
			chnget:k("chnFML2C3")

		kTrigFM3 changed chnget:k("chnFML3C1"), \
			chnget:k("chnFML3C2"), \
			chnget:k("chnFML3C3")
			
		kTrigFlt changed chnget:k("chnFltL1C1"), \
			chnget:k("chnFltL1C2"), \
			chnget:k("chnFltL1C3")

		if (kTrigFM1 == 1) then		
			reinit CHANGE_FM_OSC1
		endif

		if (kTrigFM2 == 1) then		
			reinit CHANGE_FM_OSC2
		endif

		if (kTrigFM3 == 1) then		
			reinit CHANGE_FM_OSC3
		endif
		
		if (kTrigFlt == 1) then
			reinit CHANGE_FLT
		endif
	endif

	if (iActiveOsc1 == 1) then 
		aNull subinstr "Oscillator", iCps, iAmp, 1
		aSig1 poscil3 gkOscsEnv[0], gkOscsFreq[0], giOscsWaveTable[0], giOscsPhase[0]
	else
		aSig1 init 0
	endif

	if (iActiveOsc2 == 1) then
		aNull subinstr "Oscillator", iCps, iAmp, 2
		aSig2 poscil3 gkOscsEnv[1], gkOscsFreq[1], giOscsWaveTable[1], giOscsPhase[1]
	else
		aSig2 init 0
	endif
	
	if (iActiveOsc3 == 1) then 
		aNull subinstr "Oscillator", iCps, iAmp, 3
		aSig3 poscil3 gkOscsEnv[2], gkOscsFreq[2], giOscsWaveTable[2], giOscsPhase[2]
	else
		aSig3 init 0
	endif

	;// FM	
	
	aOsc1FM poscil3 iAmp, gkOscsFreq[0] + gkOscsFMMod[0] * ((aSig1 * iFML1C1Value) + (aSig2 * iFML1C2Value) + (aSig3 * iFML1C3Value)), giOscsWaveTable[0], giOscsPhase[0]
	aOsc2FM poscil3 iAmp, gkOscsFreq[1] + gkOscsFMMod[1] * ((aSig1 * iFML2C1Value) + (aSig2 * iFML2C2Value) + (aSig3 * iFML2C3Value)), giOscsWaveTable[1], giOscsPhase[1]
	aOsc3FM poscil3 iAmp, gkOscsFreq[2] + gkOscsFMMod[2] * ((aSig1 * iFML3C1Value) + (aSig2 * iFML3C2Value) + (aSig3 * iFML3C3Value)), giOscsWaveTable[2], giOscsPhase[2]

	aOsc1FM = aOsc1FM * gkOscsEnv[0]
	aOsc2FM = aOsc2FM * gkOscsEnv[1]
	aOsc3FM = aOsc3FM * gkOscsEnv[2]

	;// FILTER

	if (iActiveFlt1 == 1) then 
		aNull subinstr "Filter", iCps, iAmp , 1
		aFlt1Low, aFlt1High, aFlt1Band svfilter (aOsc1FM * iFltL1C1Value) + (aOsc2FM * iFltL1C2Value) + (aOsc3FM * iFltL1C3Value), gkFltrsCtof[0], (gkFltrsRes[0] * 39) + 1
	else
		aFlt1Low init 0
		aFlt1Band init 0
		aFlt1High init 0
	endif

	;// INCREMENT

	vincr gaOsc1L, aOsc1FM * ( 2 * (1 - gkOscsPan[0]))
	vincr gaOsc1R, aOsc1FM * ( 2 * gkOscsPan[0])

	vincr gaOsc2L, aOsc2FM * ( 2 * (1 - gkOscsPan[1]))
	vincr gaOsc2R, aOsc2FM * ( 2 * gkOscsPan[1])

	vincr gaOsc3L, aOsc3FM * ( 2 * (1 - gkOscsPan[2]))
	vincr gaOsc3R, aOsc3FM * ( 2 * gkOscsPan[2])

	vincr gaFlt1L, ((aFlt1Low * gkFltrsLow[0]) + (aFlt1Band * gkFltrsBand[0]) + (aFlt1High * gkFltrsHigh[0]))
	vincr gaFlt1R, ((aFlt1Low * gkFltrsLow[0]) + (aFlt1Band * gkFltrsBand[0]) + (aFlt1High * gkFltrsHigh[0]))
	
endin

;//////////////////////////////////////////////////////////////////
;// Mixing and Outputs	
	
;//////////////////////////////
;// Global instrument that sums all signals, apply effects, and output them
instr +GlobalOutput

		MAIN_INIT:
			iMainVolume chnget "chnMainVolume"
			iMainPan chnget "chnMainPan"

			iMainPan = (iMainPan + 1) * 0.5

			iPanL = 2 * (1 - iMainPan)
			iPanR = 2 * (iMainPan)
		rireturn

		OUT_INIT:
			iFML1Vol chnget "chnFML1Vol"
			iFML2Vol chnget "chnFML2Vol"
			iFML3Vol chnget "chnFML3Vol"
			iFltL1Vol chnget "chnFltL1Vol"

			iFML1Pan chnget "chnFML1Pan"
			iFML2Pan chnget "chnFML2Pan"
			iFML3Pan chnget "chnFML3Pan"
			iFltL1Pan chnget "chnFltL1Pan"

			iFML1Pan = (iFML1Pan + 1) * 0.5
			iFML2Pan = (iFML2Pan + 1) * 0.5
			iFML3Pan = (iFML3Pan + 1) * 0.5
			iFltL1Pan = (iFltL1Pan + 1) * 0.5
		rireturn
		
	if (giUIUpdateDisable == 0) then
		kMainTrig changed chnget:k("chnMainVolume"), \
			chnget:k("chnMainPan")

		kOutTrig changed chnget:k("chnFML1Vol"), \
			chnget:k("chnFML2Vol"), \
			chnget:k("chnFML3Vol"), \
			chnget:k("chnFltL1Vol"), \
			chnget:k("chnFML1Pan"), \
			chnget:k("chnFML2Pan"), \
			chnget:k("chnFML3Pan"), \
			chnget:k("chnFltL1Pan")

		kFXTrig changed chnget:k("chnFXDelOnOff"), \
			chnget:k("chnFXRevOnOff")

		kDelayTrig changed chnget:k("chnFXDelTime"), \
			chnget:k("chnFXDelStOff"), \
			chnget:k("chnFXDelOnOff")

		kDelayParamTrig changed chnget:k("chnFXDelLvl"), \
			chnget:k("chnFXDelFdbk")

		kRevTrig changed chnget:k("chnFXRevDamp"), \
			chnget:k("chnFXRevOnOff")

		kRevParamTrig changed chnget:k("chnFXRevLvl"), \
			chnget:k("chnFXRevSize")

		if(kOutTrig == 1) then
			reinit OUT_INIT
		endif

		if(kMainTrig == 1) then
			reinit MAIN_INIT
		endif

		if(kDelayTrig == 1) then
			reinit DELAY_INIT
		endif
		
		if(kDelayParamTrig == 1) then
			reinit DELAY_INIT_PARAMETERS
		endif

		if(kRevTrig == 1) then
			reinit REV_INIT
		endif

		if(kRevParamTrig == 1) then
			reinit REV_INIT_PARAMETERS
		endif
	endif	

	gaMainL = (gaOsc1L * iFML1Vol * (1-iFML1Pan)) + (gaOsc2L * iFML2Vol * (1-iFML2Pan)) + (gaOsc3L * iFML3Vol * (1-iFML3Pan)) + (gaFlt1L * iFltL1Vol * (1-iFltL1Pan))
	gaMainR = (gaOsc1R * iFML1Vol * iFML1Pan)     + (gaOsc2R * iFML2Vol * iFML2Pan)     + (gaOsc3R * iFML3Vol * iFML3Pan)     + (gaFlt1R * iFltL1Vol * iFltL1Pan)

	aDelL init 0
	aDelR init 0

	aRevL init 0
	aRevR init 0

	DELAY_INIT_PARAMETERS:
		iDelayLvl  chnget "chnFXDelLvl"
		iDelayFdbk chnget "chnFXDelFdbk"
	rireturn

	DELAY_INIT:
		iDelOnOff chnget "chnFXDelOnOff"

		iDelayLvl  chnget "chnFXDelLvl"
		iDelayFdbk chnget "chnFXDelFdbk"
		iDelayTime chnget "chnFXDelTime"
		iDelayStOff chnget "chnFXDelStOff"

		if (iDelOnOff == 1) then

			aDelL = aDelL * iDelayFdbk + gaMainL * iDelayLvl
			aDelR = aDelR * iDelayFdbk + gaMainL * iDelayLvl
			
			aDelL delay aDelL, iDelayTime
			aDelR delay aDelR, iDelayTime		

			iDelayStOff = iDelayStOff + 0.5

			if (iDelayStOff > 0.5) then
				aDelL delay aDelL, abs(iDelayStOff - 0.5) + 0.001
			elseif(iDelayStOff < 0.5) then
				aDelR delay aDelR, abs(iDelayStOff - 0.5) + 0.001
			endif

			gaMainL = gaMainL + aDelL
			gaMainR = gaMainR + aDelR
		else 
			aDelL init 0
			aDelR init 0
		endif
	rireturn

	REV_INIT_PARAMETERS:
		iRevLvl  chnget "chnFXRevLvl"
		iRevSize chnget "chnFXRevSize"
	rireturn

	REV_INIT:
		iRevOnOff chnget "chnFXRevOnOff"

		iRevLvl  chnget "chnFXRevLvl"
		iRevSize chnget "chnFXRevSize"
		iRevDamp chnget "chnFXRevDamp"

		if (iRevOnOff == 1) then
			aRevL, aRevR reverbsc gaMainL + aDelL, gaMainR + aDelR, iRevSize, iRevDamp * 11025
			aRevL *= iRevLvl
			aRevR *= iRevLvl

			gaMainL = gaMainL + aRevL
			gaMainR = gaMainR + aRevR
		else
			aRevL init 0
			aRevR init 0
		endif
	rireturn
		
	gaMainL = gaMainL * iMainVolume * iPanL
	gaMainR = gaMainR * iMainVolume * iPanR

	outs gaMainL, gaMainR
	
	clear gaOsc1L
	clear gaOsc1R

	clear gaOsc2L
	clear gaOsc2R

	clear gaOsc3L
	clear gaOsc3R

	clear gaFlt1L
	clear gaFlt1R

endin

</CsInstruments>
<CsScore>
;//causes Csound to run for about 7000 years...
f0 z

i "GUIControl" 0 [3600*24*7]
i "GlobalOutput" 0 [3600*24*7]

</CsScore>
</CsoundSynthesizer>