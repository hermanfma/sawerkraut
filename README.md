# SaWerkraut

SaWerkraut is a virtual synthesizer developed with Csound language and Cabbage framework as a graduation project.

http://bdm.unb.br/handle/10483/15807

This repository contains the .csd source code, the .dll plugin (VST), presets on .fxp format.

To build it from the .csd file, use the latest version of Cabbage http://cabbageaudio.com/ and follow its instructions.

Musical demo(s):

https://soundcloud.com/hellmz/sawerkraut-demo-sideral